FROM node:14-alpine
RUN apk update && apk add --no-cache  vim

WORKDIR /src
COPY package*.json /src/
EXPOSE 9041

RUN npm install
COPY . /