
S3_BUCKET=avaamo-demo-asset/public/
echo "Uploading Public Assets to s3 Bucket: ${S3_BUCKET}"

aws s3 cp ./public s3://avaamo-demo-asset/public/  --recursive --acl public-read
echo "Uploading Public Assets to s3 Bucket Completed"

echo "-----------------------------"

echo "Dev Deploy started..."

npm run dev-deploy

echo "Dev Deploy completed."

