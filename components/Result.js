const noResults = () => {
	return `<div class="result-status bg-white">
			<h4 class="text-secondary"><b class="text-danger">⃠</b>&nbsp;&nbsp;<small>No Results Found</small></h4>
		</div>`;
};

const searchresults = () => {
	return `
	<div class="" style="text-align: center">
		<h4 class="text-secondary">Search to get Articles, Documents, News and lot more!!!</small></h4>
	</div>`;
};

module.exports = { noResults, searchresults };
