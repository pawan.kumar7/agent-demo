const { noResults } = require("./Result");

const cardTemplate = (data) => {
	return `
	<div class="card mb-3" style="width: 100%">
		<div class="card-body">
			<h5 class="card-title text-primary">${data.header}</h5>
			<p class="card-text">
				${data.summary}
			</p>
			<a href="${data.url}" class="card-link">
				View More
			</a>
		</div>
	</div>`;
};

const cards = (data) => {
	let chunks = data && data.chunks ? data.chunks : [];

	if (chunks.length === 0) return noResults();

	let answcards = [];
	let relatedcards = [];
	let answers = chunks[0];

	answers = `<p>Avaamo Answers</p>${cardTemplate(answers)}`;

	answcards.push(answers);

	chunks = chunks.slice(1, chunks.length);

	if (chunks && chunks.length > 0) {
		chunks.forEach((chunk) => {
			relatedcards.push(cardTemplate(chunk));
		});
	}

	return answcards.join("") + (relatedcards.length > 0 ? `<p>Related Contents</p>` + relatedcards.join("") : "");
};

module.exports = { cards };
