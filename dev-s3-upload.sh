S3_BUCKET=avaamo-demo-asset/public/

echo "Uploading Public Assets to s3 Bucket: ${S3_BUCKET}"

aws s3 cp ./public s3://${S3_BUCKET}  --recursive --acl public-read
echo "Uploading Public Assets to s3 Bucket Completed"