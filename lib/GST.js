const fetch = require("node-fetch");
const Answers = require("./Answers");
const { getUrls } = require("../helpers/utils");

const { ESURL, TOKEN, ESUSER } = process.env;

class GST {}

const requestAsync = async (url) => {
	return new Promise((resolve, reject) => {
		fetch(url)
			.then((res) => {
				if (res.status === 200) {
					try {
						res
							.json()
							.then((resp) => {
								console.log(`Fetched Successfully: ${url}`);
								resolve({
									data: resp
								});
							})
							.catch((err) => {
								console.log(`Fetched Error: ${url}`);
								resolve({ data: {} });
							});
					} catch (err) {
						console.log(`Fetched Error: ${url}`);
						resolve({
							data: {}
						});
					}
				} else {
					resolve({
						data: {}
					});
				}
			})
			.catch((err) => {
				console.log(`Fetched Error: ${url}`, "\n" + err);
				resolve({
					data: {}
				});
			});
	});
};

const mapURL = (query, facet_type = "") => {
	query = encodeURIComponent(query);
	if (facet_type) return `${ESURL}?q=${query}&user=${ESUSER}&token=${TOKEN}&facet_type=${facet_type}`;
	return `${ESURL}?q=${query}&user=${ESUSER}&token=${TOKEN}`;
};

const getAsyncEsData = async (query, facet_type = "") => {
	console.log("query", query);
	let urls = query.map((i) => mapURL(i, facet_type));
	console.log("\nnew_url>>", urls);

	let response = await Promise.all(urls.map((url) => requestAsync(url)));
	//console.log("\nesresp_fun>>", response);
	if (!response || Object.keys(response).length === 0) return [];
	let t = [];
	response = response.map((resp) => t.push(resp.data));
	response = t;
	//console.log("\nresponse>>",response)

	// urls = getUrls(response);
	// if (urls && urls.length === 0) {
	// 	return [];
	// }
	return response;
};

GST.searchResult = async (query, facet_type) => {
	try {
		// query = await Answers.getKeywords(query);
		query = query || "";
		let response = await getAsyncEsData(query, facet_type);
		console.log("\nurls>>\n", response);
		return response;
	} catch (error) {
		console.log("error", error);
		throw new Error(error);
	}
};

module.exports = GST;
