const fetch = require("node-fetch");
const { QUERY_KEYWORDS, AVAAMO_ANSWERS } = process.env;

class Answers {}

const createOptions = (query, esdata) => {
	let params = [];
	//console.log("\nES_URLS>>", urls);

	let documents = esdata;

	documents.map((doc) =>
		params.push({
			doc_id: Math.floor(Math.random() * 1000000),
			kp_id: 8773,
			template: {
				template_json: {
					body_contents: ["h1", "h2", "h3", "h4", "h5", "p", "img", "figure", "ol", "ul", "dd", "dt", "a", "span"]
				}
			},
			language: "en-US",
			url: doc.uri,
			title: Array.isArray(doc.title) ? doc.title[0] : doc.title,
			description: doc.abstract ? doc.abstract + " ... " : "" + doc.content
		})
	);

	return {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			params: params,
			query: query,
			configuration: {
				answer: false
			}
		})
	};
};

Answers.getKeywords = async (query) => {
	let url = QUERY_KEYWORDS + "?version=v2&query=" + encodeURIComponent(query);
	//console.log("\nkeywords-url", url);
	let resp = await fetch(url)
		.then((resp) => resp.json())
		.catch((err) => {
			console.log("\nError keywords:", err);
			return {};
		});

	console.log("\nresp_key>", resp);

	if (resp && resp.error) {
		context.variables.keywords_resp = [];
		context.variables.entities = [];
		return query;
	}

	console.log("\nkeywords>>", resp.keywords);
	console.log(
		"\nentities>>",
		resp.phrases.map((i) => i.text)
	);

	let entities = [];
	if (Object.keys(resp).length === 0) {
		entities.push(resp.extra_keywords.join(" "));
	} else if (resp && resp.keywords.length === 1) {
		entities.push(resp.keywords.join(" "));
		resp.phrases.map((i) => entities.push(i.text));
	} else {
		resp.phrases.map((i) => entities.push(i.text));
		entities.push(resp.keywords.join(" "));
	}
	return entities;
};

Answers.getAnswers = async (esdata, query) => {
	let options = createOptions(esdata, query);
	console.log("\noptions>>", options);
	let answers_data = await fetch(AVAAMO_ANSWERS, options)
		.then(function (response) {
			console.log(response.status);
			if (response.status === 200) {
				return response.json();
			}
			return [];
		})
		.catch(function (error) {
			// if there's an error, log it
			console.log("\nError catch:", error);
			return [];
		});
	//console.log('\nanswers_data:', answers_data)
	return answers_data;
};

module.exports = Answers;
