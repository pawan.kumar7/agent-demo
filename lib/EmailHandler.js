const sgMail = require("@sendgrid/mail");
const fs = require("fs");
const { NODE_ENV, SENDGRID_API_KEY } = process.env;
const nodemailer = require("nodemailer");
sgMail.setApiKey(SENDGRID_API_KEY);

class EmailHandler {
	constructor({ config, type = "smtp" }) {
		this.type = type;
		this.mailConfig = config;
	}
	async mail() {
		if (this.type != "smtp" && NODE_ENV === "development") {
			// this.mailConfig.from = "pawan.kumar@avaamo.com";
			return await sgMail.send(this.mailConfig, (error, result) => {
				if (error) {
					// couldn't send email
					//Do something with the error
					console.log(error.code, error.response.body);
					return false;
				} else {
					console.log("result:", result);
					//email sent
					return true;
				}
			});
		} else {
			// send mail
			return new Promise((resolve, reject) => {
				let transport = nodemailer.createTransport({
					host: "smtp.internal.ericsson.com",
					port: 25,
					secure: false,
					tls: { rejectUnauthorized: false }
				});

				transport.sendMail(this.mailConfig, function (error, info) {
					console.log("Sending mail...");
					if (error) {
						console.log("error is " + error);
						resolve(false); // or use rejcet(false) but then you will have to handle errors
					} else {
						console.log("Email sent: " + info.response);
						resolve(true);
					}
				});
			});
		}
	}
}

module.exports = EmailHandler;
