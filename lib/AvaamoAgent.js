const fetch = require("node-fetch");
const { v4: uuidv4 } = require("uuid");
class AvaamoAgent {
	constructor({ url, accessToken, channel_uuid, first_name, last_name, custom_properties }) {
		this.url = url;
		this.accessToken = accessToken || "";
		this.channel_uuid = channel_uuid;
		this.first_name = first_name || "John";
		this.last_name = last_name || "Doe";
		this.custom_properties = custom_properties || { app_channel: "custom" };
	}

	async sendMessage(message) {
		let payload = {
			channel_uuid: this.channel_uuid,
			user: {
				first_name: this.first_name,
				last_name: this.last_name,
				uuid: uuidv4()
			},
			message: {
				text: message
			}
		};
		console.log("Payload>>", payload, this.url);
		return await fetch(this.url, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Access-Token": "fadc5c269d854a98a6450fc08cf45b05"
			},
			body: JSON.stringify(payload)
		})
			.then((res) => res.json())
			.then(async function (res) {
				return await res;
			})
			.catch((err) => {
				console.log("Error:", err);
				throw new Error("Technical Error");
			});
	}
}

module.exports = AvaamoAgent;
