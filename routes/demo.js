var express = require("express");
var _ = require("lodash");
var router = express.Router();

const AvaamoAgent = require("../lib/AvaamoAgent");
const { cards } = require("../components/Card");
const { searchresults } = require("../components/Result");
const data = require("../data/answers.json");

const url = "https://c3.avaamo.com/bot_connector_webhooks/s-t-3859576e-a1c1-4552-84dc-083fd2821017/message.json";
const channel_uuid = "51d78010-0690-44b2-abc4-e83572ac3ad9";
const accessToken = "fadc5c269d854a98a6450fc08cf45b05";

const customAgent = {
	url: url,
	channel_uuid: channel_uuid,
	accessToken: accessToken
};

router.get("/", (req, res, next) => {
	res.render("demo", {
		info: process.env.AGENT_TITLE,
		description: process.env.AGENT_DESCRIPTION,
		sub_title: process.env.AGENT_SUB_TITLE,
		src: `<script type="text/javascript" async src="${process.env.SRC}"></script>`,
		src_url: process.env.SRC,
		header: process.env.HEADER,
		ver: "1.0.0"
	});
});

router.get("/demom2", (req, res, next) => {
	res.render("demom2", {
		info: process.env.AGENT_TITLE,
		description: process.env.AGENT_DESCRIPTION,
		sub_title: process.env.AGENT_SUB_TITLE,
		src: `<script type="text/javascript" async src="${process.env.SRC}"></script>`,
		src_url: process.env.SRC,
		header: process.env.HEADER,
		ver: "1.0.0"
	});
});

router.get("/m2", (req, res, next) => {
	res.render("m2", {
		info: process.env.AGENT_TITLE,
		description: process.env.AGENT_DESCRIPTION,
		sub_title: process.env.AGENT_SUB_TITLE,
		src: `<script type="text/javascript" async src="${process.env.SRC}"></script>`,
		src_url: process.env.SRC,
		header: process.env.HEADER,
		ver: "1.0.0"
	});
});

router.get("/demo1", (req, res, next) => {
	res.render("demo1");
});

module.exports = router;
