var express = require("express");
var _ = require("lodash");
var router = express.Router();
const createError = require("http-errors");
const EmailHandler = require("../lib/EmailHandler");

/* GET home page. */
router.get("/", async (req, res, next) => {
	console.log("GET home page");
	// res.send({ info: process.env.AGENT_TITLE, description: process.env.AGENT_DESCRIPTION, ver: "1.0.0" }).status(200);
	res.send({ info: "Agent Demo" });
});

router.get("/voicedemo", async (req, res, next) => {
	res.render("voice");
});

router.get("/smp", async (req, res, next) => {
	res.render("ericsmp");
});

router.get("/mee", async (req, res, next) => {
	res.render("meeDemo");
});

router.post("/sendTicket", async (req, res, next) => {
	let body = req.body;
	console.log("sendTicket body: " + JSON.stringify(body));

	let { emailAddress, firstName, lastName, application, description } = req.body;

	let msg = {
		to: emailAddress || ["pawan.kumar@avaamo.com"],
		from: "pawan.kumar@avaamo.com",
		subject: `SMP Ticket Test`,
		text: `SMP Ticket Test`,
		html: `SMP Ticket Test HTML Body`
	};

	res.send({
		message: "success"
	});

	// let sendmail = new EmailHandler({ config: msg, type: "sendGrid" });
	// sendmail = await sendmail.mail();
	// console.log("sendmail:", sendmail);
	// if (sendmail) {
	// 	console.log(`\nMail sent Successfully`);
	// 	res.send({
	// 		message: "success"
	// 	});
	// } else {
	// 	console.log(`\nTechnical error sending mail`);
	// 	res.send({
	// 		message: "failed"
	// 	});
	// }
});

module.exports = router;
