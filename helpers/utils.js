const { ESURL, TOKEN, ESUSER } = process.env;

const mapURL = (query, facet_type = "") => {
	console.log("env>>", ESURL, TOKEN, USER);
	query = encodeURIComponent(query);
	if (facet_type) return `${ESURL}?q=${query}&user=${ESUSER}&token=${TOKEN}&facet_type=${facet_type}`;
	return `${ESURL}?q=${query}&user=${ESUSER}&token=${TOKEN}`;
};

const getUrls = (data) => {
	let urls = [];
	//console.log(`\ndata>>\n${JSON.stringify(data)}`);
	data.map((resp) => {
		if (resp.documentList && resp.documentList.documents && resp.documentList.documents.length > 0) {
			let temp = [];
			resp.documentList.documents.map((document) => {
				if (document.secondary_hits && document.secondary_hits.length > 0) {
					document.secondary_hit.map((hits) => temp.push(hits));
				}
			});

			temp.map((sec_hits) => resp.documentList.documents.push(sec_hits));

			let url = resp.documentList.documents
				.map((doc) => {
					if (domainFilter(doc.uri)) {
						return doc.uri;
					}
					return "";
				})
				.filter((i) => i !== "");

			url.map((i) => urls.push(i));
		}
	});

	return urls;
};

module.exports = { mapURL, getUrls };
