var AvaamoChatBot = function (t) {
	function o(t, o) {
		var n = document.createElement("script");
		n.setAttribute("src", t), n.setAttribute("id", "avm-web-channel"), (n.onload = o), document.body.appendChild(n);
	}
	return (
		(this.options = t || {}),
		(this.load = function (t) {
			o(this.options.url, function () {
				window.Avaamo.addFrame(), t && "function" == typeof t && t(window.Avaamo);
			});
		}),
		this
	);
};
