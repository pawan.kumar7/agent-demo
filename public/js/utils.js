function varifyName(elem) {
	let value = elem.value;
	let regex = /^[a-zA-Z]+$/;
	if (value && value.length >= 3 && regex.test(value)) toggleValidAttr(elem);
	else toggleInValidAttr(elem);
}

function isEmail(email) {
	var regex =
		/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (email.value.trim().length > 0 && regex.test(email.value.trim())) toggleValidAttr(email);
	else toggleInValidAttr(email);
}

function isPhoneNumber(phone) {
	let regex = /^\+([0-9]{2})\)?([0-9]{10})$/;
	console.log(regex.test(phone.value));
	if (phone.value.length > 0 && regex.test(phone.value)) toggleValidAttr(phone);
	else toggleInValidAttr(phone);
}

function toggleInValidAttr(elem) {
	//is-valid, is-invalid
	elem.classList.remove("is-valid");
	elem.classList.add("is-invalid");
}

function toggleValidAttr(elem) {
	elem.classList.remove("is-invalid");
	elem.classList.add("is-valid");
}

function form_validate(attr_id) {
	var result = true;
	// $("#" + attr_id).validator("validate");
	$("#" + attr_id + " .form-group").each(function () {
		if ($(this).hasClass("is-invalid")) {
			console.log($(this));
			result = false;
			return false;
		}
	});
	return result;
}

function formSubmit(attr_id) {
	event.preventDefault();

	console.log(form_validate(attr_id));

	if (form_validate(attr_id)) {
		var elements = document.getElementById(attr_id).elements;
		var obj = {};
		for (var i = 0; i < elements.length; i++) {
			// console.log(elements.item(i));
			var item = elements.item(i);
			obj[item.name] = item.value;
		}
		console.log(obj);
		$.post("/sendTicket", obj, function (data) {
			// alert(typeof data, JSON.stringify(data));
			if (data && data.message === "success") {
				alert("Mail sent successfully");
			} else {
				alert("Mail sent failed");
			}
		});
	}
}

/**
 * MEE JS
 */

var selectedData = [];
$(document).ready(function () {
	let options = data.map((item) => displayInputFields(item)).join(" ");
	document.getElementById("options").innerHTML = options;
});

function getSelectedCheckboxValues() {
	let checkedItems = data.filter((i) => i.checked);
	console.log(checkedItems);
}

function selectAll() {
	let selectStat = document.getElementById("flexCheckDefault").checked;
	data.forEach((cb) => {
		cb.checked = selectStat;
	});
	let options = data.map((item) => displayInputFields(item)).join(" ");
	document.getElementById("options").innerHTML = options;
}

function checkItem(id) {
	let index = data.findIndex((i) => i.DEPARTMENT_ID == id);
	index ? (data[index].checked = !data[index].checked) : null;

	let checkedItems = data.filter((i) => i.checked);
	let options = checkedItems.map((item) => "<span style='mr-3'>" + item.DEPARTMENT + " <span>").join(" ");
	document.getElementById("selected-items").innerHTML = options;
}

function displayInputFields(item) {
	return item.checked
		? `<div class="input-group mb-1">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<input type="checkbox" name="dept" value='${item.DEPARTMENT}' onchange="checkItem(${item.DEPARTMENT_ID})" id='${item.DEPARTMENT_ID}' checked/>
						</div>
					</div>
					<label class="form-control">${item.DEPARTMENT}</label>
				</div>`
		: `<div class="input-group mb-1">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<input type="checkbox" name="dept" value='${item.DEPARTMENT}' onchange="checkItem(${item.DEPARTMENT_ID})" id='${item.DEPARTMENT_ID}'/>
						</div>
					</div>
					<label class="form-control">${item.DEPARTMENT}</label>
				</div>`;
}

function onSearch() {
	let searchinput = document.getElementById("search").value.toLowerCase();
	// console.log(searchinput);
	let data1 = data.filter((i) => i.DEPARTMENT.toLowerCase().includes(searchinput));
	let options = data1.map((item) => displayInputFields(item)).join(" ");
	document.getElementById("options").innerHTML = options;
}

function clearAll() {
	let selectStat = (document.getElementById("flexCheckDefault").checked = false);
	data.forEach((cb) => {
		cb.checked = false;
	});
	let options = data.map((item) => displayInputFields(item)).join(" ");
	document.getElementById("options").innerHTML = options;
}
