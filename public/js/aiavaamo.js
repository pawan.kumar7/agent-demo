"use strict";
// Globals
var recording = false;

//connection to socket
const socket = io.connect("https://voice.aiavaamo.com");

//================= CONFIG =================
// Stream Audio
var bufferSize = 2048,
	context,
	processor,
	input,
	recorder,
	globalStream;

//vars
var audioElement = document.querySelector("audio"),
	finalWord = false,
	resultText = document.getElementById("ava-showText"),
	removeLastSentence = true,
	paused = false,
	initialized = false,
	streamStreaming = false;

//audioStream constraints
const constraints = {
	audio: true,
	video: false
};
var avaamoObject;
var popup;
//================= RECORDING =================
function initRecording() {
	navigator.mediaDevices
		.getUserMedia(constraints)
		.then(function (stream) {
			recorder = RecordRTC(stream, {
				timeSlice: 1000,
				bufferSize: 4096,
				desiredSampRate: 16000,
				type: "audio",
				mimeType: "audio/wav",
				recorderType: StereoAudioRecorder,
				numberOfAudioChannels: 1,
				onAudioProcessStarted: function () {},
				ondataavailable: function (blob) {
					var fileReader = new FileReader();
					fileReader.onload = function () {
						socket.emit("binaryData", this.result);
					};
					fileReader.readAsArrayBuffer(blob);
				}
			});
			startRecording();
		})
		.catch(function (error) {
			alert(JSON.stringify(error));
		});
}

function microphoneProcess(e) {
	var left = e.inputBuffer.getChannelData(0);
	var left16 = downsampleBuffer(left, 44100, 16000);
	socket.emit("binaryData", left16);
}

function toggleRecording() {
	if (!recording) {
		recording = true;
		startRecording();
	} else {
		recording = false;
		streamStreaming = false;
		socket.emit("endGoogleCloudStream", "");
		stopRecording();
	}
}

function startRecording() {
	console.log("In START recording");
	if (!initialized) {
		initialized = true;
		initRecording();
	} else {
		recording = true;
		if (paused) {
			recorder.resumeRecording();
		} else {
			recorder.startRecording();
		}
		socket.emit("startGoogleCloudStream", "");
	}
}

function stopRecording() {
	recording = false;
	paused = true;
	recorder.pauseRecording();
	console.log("IN STOP RECORDING", document.getElementById("stspan"));
	socket.emit("endGoogleCloudStream", "");
}

// Language and Hints

function languageChanged(lang) {
	socket.emit("languageChanged", lang);
}

function addHints(hints) {
	socket.emit("hints", hints);
}

// Event handlers
function micClickHandler(e) {
	stopSpeaking();
	intermediateText("toggle");
	toggleRecording();
}

// Callback on finalization
function speechFinalizedHandler(f) {
	window.Avaamo.speechFinalized = f;
}

/**
 *
 * @param {*} n The element overwhich other UI elements need to be created
 */
function createInterfaceOnElement(n, a) {
	avaamoObject = a;
	popup = n;
	let b = baseInterface(a);
	n.appendChild(b);
	// Adding mic on base interface instead of here
	// b.appendChild(interfaceForMicrophone());
	n.appendChild(interfaceForIntermediateText());
	n.appendChild(interfaceForAudio(a));
	n.appendChild(interfaceForNetworkWarning());

	// Interface properties
	a.mic = document.getElementById("custom-mic");
	a.stt = document.getElementById("stt");
	a.audio = document.getElementById("avaamoaudio");
	a.networkWarning = networkWarning;

	// Registering handlers
	a.mic.addEventListener("click", micClickHandler);

	// Functions
	a.voiceReplacements = (t) => t;
	a.addVoiceHints = (hintsArray) => socket.emit("hints", hintsArray);

	a.userInteracted = false;
	a.onUserMessage = (m) => {
		console.log(`User message: ${m}`);
		a.userInteracted = true;
	};
}

function baseInterface(a) {
	let editorArea = document.createElement("div");
	// Area
	editorArea.classList.add("editor-area");
	editorArea.style.width = document.querySelector("#avaamo__popup").offsetWidth.toString() + "px";
	editorArea.style.height = "43px";
	editorArea.style.position = "absolute";
	editorArea.style.bottom = "0";
	editorArea.style.right = "0";

	// editorArea.style.backgroundColor = 'red';

	// Wrapper
	let composeMessageFields = document.createElement("div");
	composeMessageFields.classList.add("compose-message-fields");
	editorArea.appendChild(composeMessageFields);

	// Textarea wrap
	let textareaWrap = document.createElement("div");
	textareaWrap.classList.add("textarea-wrap", "alignItemsCenter");
	textareaWrap.style.display = "flex";
	textareaWrap.style.marginRight = "10px";
	composeMessageFields.appendChild(textareaWrap);

	// Text area
	let textarea = document.createElement("textarea");
	textarea.classList.add("txt", "anima", "edit", "local", "type-msg");
	textarea.dir = "auto";
	textarea.name = "message";
	textarea.style.lineHeight = "16px";
	textarea.style.height = "20px";
	textarea.style.overflowX = "hidden";
	textarea.style.overflowY = "auto";
	textarea.style.width = "100%";
	textarea.style.fontSize = "14px";
	textarea.style.resize = "none";
	textarea.style.border = "0";
	textarea.style.margin = "0";
	textarea.style.padding = "10px";
	textarea.style.boxShadow = "none";
	textarea.style.outline = "none";
	// textarea.style.background = 'transparent';
	textarea.placeholder = "Type a message...";
	textarea.addEventListener("keypress", (e) => {
		if (e.which == 13) {
			// console.log('TEXT AREA ',textarea.value);
			a.sendMessage(textarea.value);
			a.onUserMessage(textarea.value);

			setTimeout(function () {
				textarea.value = "";
			}, 1);
		}
	});
	textareaWrap.appendChild(textarea);

	// Mic
	textareaWrap.appendChild(interfaceForMicrophone());

	// Send
	let send = document.createElement("div");
	send.classList.add("ptr", "locale-trans", "sendChange");
	send.style.cursor = "pointer";
	send.style.fontSize = "15px";
	send.style.lineHeight = "40px";
	send.innerText = "Send";
	send.addEventListener("click", (event) => {
		a.sendMessage(textarea.value);
		a.onUserMessage(textarea.value);
		setTimeout(function () {
			textarea.value = "";
		}, 1);
	});
	textareaWrap.appendChild(send);

	// Keyboard
	let keyb = document.createElement("div");
	keyb.classList.add("keyboard");
	keyb.style.cursor = "pointer";
	textareaWrap.appendChild(keyb);

	return editorArea;
}

function interfaceForMicrophone() {
	let mic = document.createElement("div");
	mic.classList.add("custom-mic", "mic", "ptr");
	mic.id = "custom-mic";

	return mic;
}

function interfaceForIntermediateText() {
	let div = document.createElement("div");
	div.classList.add("user-text");
	div.id = "speech-text";

	let span = document.createElement("span");
	span.id = "stt";
	span.innerText = "I am listening...";
	div.appendChild(span);
	return div;
}

function intermediateText(d, t = "I am listening...") {
	console.log("In intermediateText", d);
	let div = document.getElementById("speech-text");
	// div.style.display = "none";
	let stt = document.getElementById("stt");
	let mic = window.Avaamo.mic;
	stt.innerText = t;
	if (d === "show") {
		div.style.display = "block";
		if (!mic.classList.contains("enabled")) mic.classList.add("enabled");
	} else if (d === "hide") {
		console.log("IN 2ND IF");
		div.style.display = "none";
		if (mic.classList.contains("enabled")) mic.classList.remove("enabled");
	} else if (d === "toggle") {
		if (div.style.display === "none" || !div.style.display) div.style.display = "block";
		else if (div.style.display === "block") div.style.display = "none";
		mic.classList.toggle("enabled");
	}
}

function interfaceForAudio(a) {
	a.mute = false;
	let audio = document.createElement("audio");
	audio.id = "avaamoaudio";
	audio.addEventListener("ended", () => {
		// Audio finished playing
	});
	return audio;
}

function interfaceForNetworkWarning() {
	let snackbar = document.createElement("div");
	snackbar.id = "snackbar";
	snackbar.innerHTML = "Poor Network Connection";
	return snackbar;
}

function networkWarning(d) {
	let snackbar = document.getElementById("snackbar");
	if (d === "show") snackbar.style.display = "block";
	else if (d === "hide") snackbar.style.display = "none";
}

//================= SOCKET IO =================
socket.on("connect", function (data) {
	socket.emit("join", "Server Connected to Client");
});

socket.on("messages", function (data) {});

socket.on("speechData", function (data) {
	//start-end
	var dataFinal = undefined || data.results[0].isFinal;
	let wholeString = data.results[0].alternatives[0].transcript;
	if (dataFinal === false) {
		window.Avaamo.stt.classList.add("blurred");
		window.Avaamo.stt.innerText = wholeString;
	} else if (dataFinal === true) {
		// window.Avaamo.speechFinalized(dataFinal);
		window.Avaamo.stt.innerText = wholeString;

		var mic = document.querySelector(".custom-mic.mic.ptr.enabled");
		if (mic) {
			setTimeout(function () {
				// Run the recognized string through replacements
				wholeString = Avaamo.voiceReplacements(wholeString);

				Avaamo.sendMessage(wholeString);
				Avaamo.onUserMessage(wholeString);
				this.Avaamo.stt.classList.remove("blurred");
				toggleRecording();
				intermediateText("hide");
			}, 0);
		} else {
			window.Avaamo.stt.classList.remove("blurred");
		}
	}
});

window.onbeforeunload = function () {
	if (streamStreaming) {
		socket.emit("endGoogleCloudStream", "");
	}
};

//================= SANTAS HELPERS =================
// sampleRateHertz 16000 //saved sound is awefull
function convertFloat32ToInt16(buffer) {
	let l = buffer.length;
	let buf = new Int16Array(l / 3);

	while (l--) {
		if (l % 3 == 0) {
			buf[l / 3] = buffer[l] * 0xffff;
		}
	}
	return buf.buffer;
}

var downsampleBuffer = function (buffer, sampleRate, outSampleRate) {
	if (outSampleRate == sampleRate) {
		return buffer;
	}
	if (outSampleRate > sampleRate) {
		throw "downsampling rate show be smaller than original sample rate";
	}
	var sampleRateRatio = sampleRate / outSampleRate;
	var newLength = Math.round(buffer.length / sampleRateRatio);
	var result = new Int16Array(newLength);
	var offsetResult = 0;
	var offsetBuffer = 0;
	while (offsetResult < result.length) {
		var nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
		var accum = 0,
			count = 0;
		for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
			accum += buffer[i];
			count++;
		}

		result[offsetResult] = Math.min(1, accum / count) * 0x7fff;
		offsetResult++;
		offsetBuffer = nextOffsetBuffer;
	}
	return result.buffer;
};

//================= SPEECH =================

/**
 * Parses message to be sent to speech synthesizer
 * @param {*} c message from Avaamo.onBotMessage
 */
function parseContent(c, linkread = false) {
	let content = "";
	if (c.attachments != null && c.attachments.card_carousel) {
		let firstCard = c.attachments.card_carousel.cards[0];
		content = firstCard.description;
		content = content
			.replace(/<br>/g, "")
			.replace(/<b>/g, "")
			.replace(/<\/b>/g, "")
			.replace(/<br\/>/g, "");
		content = content.replace(/\.(?!\s)/g, ". ");
	} else if (c.attachments != null && c.attachments.default_card) {
		// CARD
		// Remove iframe if any found
		if (c.attachments.default_card.description.indexOf("<iframe") > -1) {
			let desc = c.attachments.default_card.description;
			let pre = desc.split("<iframe")[0];
			desc = desc.split("<iframe")[1];
			let post = desc.split("iframe>")[1];
			desc = pre + post;
			if (desc.length == 0) {
				desc = "Please check the content for more information.";
			}
			c.attachments.default_card.description = desc;
		}

		content = c.attachments.default_card.description + ".";
		content = content
			.replace(/<br>/g, "")
			.replace(/<b>/g, "")
			.replace(/<\/b>/g, "")
			.replace(/<br\/>/g, "");
		content = content.replace(/\.(?!\s)/g, ". ");
		if (linkread) {
			if (c.attachments.default_card.links && c.attachments.default_card.links.length > 0) {
				for (var i = 0; i < c.attachments.default_card.links.length; i++) {
					content += c.attachments.default_card.links[i].title + " . ";
				}
			}
		}
	} else if (c.attachments != null && c.attachments.quick_reply) {
		// QUICK REPLY
		content = c.content;
		for (var i = 0; i < c.attachments.quick_reply.quick_links.length; i++) {
			content += c.attachments.quick_reply.quick_links[i].title + " . ";
		}
	} else {
		// TEXT
		content = c.content;
	}
	return content;
}

/**
 * Stop speaking
 */
function stopSpeaking() {
	let audio = window.Avaamo.audio;
	audio.pause();
	audio.currentTime = 0;
}

// Queueing mechanism for speech
var queue = [];
var currentHead = 0;

/**
 * Queues the messages before parsing and playing it
 * If a new message sequence is encountered, the queue will be reset
 * Refer for persona and locale - https://docs.aws.amazon.com/polly/latest/dg/voicelist.html
 * @param {*} m Message to be queued
 * @param {*} persona Persona to be spoken with. Defaults to 'Joanna'
 * @param {*} locale Locale to be spoken in. Defaults to 'en-US
 */
async function queueSpeech(m, persona = "Joanna", locale = "en-US") {
	console.log("/start Queue");
	console.log(JSON.stringify(queue));

	if (!window.Avaamo.userInteracted) {
		if (queue.length > 0) {
			queue = [];
		}
	}

	if (queue.length == 0) {
		queue.push(m);
	} else if (m.request_message_uuid == queue[0].request_message_uuid) {
		queue.push(m);
	} else {
		queue = [];
		queue.push(m);
	}

	let sequence = m.sequence.split("/");
	if (sequence[0] == sequence[1]) {
		// Start playing
		currentHead = 0;
		await playQueue(persona, locale);
	}
	console.log("/end Queue");
}

async function playQueue(persona, locale) {
	console.log(`/start Play queue`);
	if (queue[currentHead]) {
		speak(await getTTSAudio(parseContent(queue[currentHead]), persona, locale), persona, locale);
	}
	console.log(`/end Play queue`);
}

/**
 * Sanitizes a string by removing any html tags
 * @param {*} html String to sanitize
 */
function stripHTML(html) {
	var tmp = document.createElement("div");
	tmp.innerHTML = html;
	return tmp.textContent || tmp.innerText || "";
}

/**
 * Gets an audio file systhesized with speech using provided persona and locale
 * Refer - https://docs.aws.amazon.com/polly/latest/dg/voicelist.html
 * @param {*} t Text to synthesize the audio for
 * @param {*} persona Persona used to synthesize the text
 * @param {*} locale Locale used to synthesize the text
 */
async function getTTSAudio(t, persona, locale) {
	// Strip tags before proceeding
	t = stripHTML(t);
	// Strip unicode before proceeding
	t = t.replace("🎥", "");
	console.log(`Getting TTS: ${t}`);
	let payload = {
		ssml: `<speak>${t}</speak>`,
		persona: persona,
		locale: locale
	};
	let speech = await fetch("https://c3.avaamo.com/web_channels/ssml_to_voice.json/", {
		method: "POST",
		body: JSON.stringify(payload),
		headers: {
			"Content-Type": "application/json"
		}
	}).then((res) => res.json());
	speech.text = t;
	return speech;
}

/**
 * Plays the speech audio
 * @param {*} speech The audio for the synthesized speech
 * @param {*} persona Persona used to speak the text
 * @param {*} locale Locale used to speak the text
 */
function speak(speech, persona, locale) {
	console.log(`Speak: ${speech.text}`);
	// Speak only if mic is not recording
	if (!recording) {
		let audio = window.Avaamo.audio;
		audio.src = speech.location;
		let playerPromise = audio.play();
		if (playerPromise) {
			playerPromise
				.then((p) => {})
				.catch((e) => {
					console.error(e);
				});
		}
		window.Avaamo.speaking = true;
		audio.onended = function () {
			window.Avaamo.speaking = false;
			// Go to the next track and play
			currentHead++;
			playQueue(persona, locale);
		};
	}
}
